﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LoggingService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SitecoreMicroservice.Models;
using SitecoreMicroservice.Services;

namespace SitecoreMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SitecoreController : ControllerBase
    {
        private static LoggerService Logger;
        
        public SitecoreController()
        {
            Logger = new LoggerService("Sitecore");
        }
        [HttpGet]
        public string Get()
        {
            const string message = "Sitecore microservice started .....";
            return message;
        }
        /// <summary>
        /// Deletes a provider from sitecore
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteProvider")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public async Task<ActionResult> DeleteProvider([FromHeader(Name = "id")] string id, [FromHeader(Name="user")] string user, [FromHeader(Name="url")] string url)
        {
            try
            {
                if (user == null || url == null) return BadRequest("NO providers passed...");
                // login firs
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);
                var providerId = JsonConvert.DeserializeObject<string>(id);
                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }
                success = await DeleteProvider(providerId, dataService);
                await Logout(dataService);
                if (success)
                    Logger.WriteInfo(id + " Deleted successfully");
                else
                {
                    Logger.WriteError(id + " Unable to delete provider");
                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Exception occurred " + e.Message);
            }
        }
        /// <summary>
        /// Gets children with standard fields
        /// </summary>
        /// <param name="user"></param>
        /// <param name="url"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetChildrenWithStandardFields")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public async Task<ActionResult> GetChildrenWithStandardFields([FromHeader(Name = "user")] string user, [FromHeader(Name = "url")] string url, [FromHeader(Name="id")] string parentId)
        {
            try
            {
                if (user ==null || url ==null || parentId ==null) return BadRequest("NO providers passed...");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);
                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }
                var id = parentId.ToString();
                var result = await GetChildrenWithStandardFields(url, id, dataService);
                if (result == null) return BadRequest("No Children returned");
                await Logout(dataService);
                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred :: GetChyildrenWithStandardFields :: " + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }
        private async Task<string> GetChildrenWithStandardFields(string baseUrl, string parentId, DataService dataService)
        {
            var itemResult = await dataService.Get(baseUrl + "sitecore/api/ssc/item/" + parentId +
                                                   "/children?database=master&includeStandardTemplateFields=true");
            //var itemResult = await client.GetAsync("sitecore/api/ssc/item/" + parentId + "/children?database=master&includeStandardTemplateFields=true");
            if (!itemResult.IsSuccessStatusCode) return null;
            var result = itemResult.Content.ReadAsStringAsync().Result;
            return result;
        }
       /// <summary>
       /// Gets children
       /// </summary>
       /// <param name="user"></param>
       /// <param name="url"></param>
       /// <param name="parentId"></param>
       /// <returns></returns>
        [HttpGet]
        [Route("GetParentChildren")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public async Task<ActionResult> GetParentChildren([FromHeader(Name = "user")] string user, [FromHeader(Name = "url")] string url, [FromHeader(Name = "id")] string parentId)
        {
            try
            {
                if (user == null || url == null || parentId == null) return BadRequest("NO providers passed...");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);
                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }
                var id = parentId.ToString();
                var result = await GetChildren(url, id, dataService);
                if (result == null) return BadRequest("No Children returned");
                await Logout(dataService);
                return new JsonResult(result);

            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred :: GetParentChildren :: " + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }
       [HttpGet]
       [Route("GetItemById")]
       [ProducesResponseType(StatusCodes.Status200OK)]
       [ProducesResponseType(StatusCodes.Status400BadRequest)]
       [Produces("application/json")]
        public async Task<ActionResult> GetItemById([FromHeader(Name = "user")] string user, [FromHeader(Name = "url")] string url, [FromHeader(Name = "id")] string id)
        {
            try
            {
                if (user ==null || url ==null || id == null) return BadRequest("Missing input values....");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);
                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }

                var parentId = id.ToString();
                var item = await GetItemById(url, parentId, dataService);
                var result = await item.Content.ReadAsStringAsync();
                await Logout(dataService);
                return item.StatusCode switch
                {
                    HttpStatusCode.NotFound => NotFound(result),
                    HttpStatusCode.OK => Ok(result),
                    _ => BadRequest(result)
                };
            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred " + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }
       [HttpGet]
       [Route("GetItemByPath")]
       [ProducesResponseType(StatusCodes.Status200OK)]
       [ProducesResponseType(StatusCodes.Status400BadRequest)]
       [Produces("application/json")]
        public async Task<ActionResult> GetItemByPath([FromHeader(Name="user")] string user, [FromHeader(Name="url")] string url, [FromHeader(Name="parentPath")] string parentPath, [FromHeader(Name="newId")] string newId)
        {
            try
            {
                if (user == null || url ==null || parentPath==null || newId==null) return BadRequest("Missing input parameters");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);

                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }
                //Logger.Info("Authentication successful");
                var id = newId.ToString();
                var result = await GetItemByPath(url, parentPath, id, dataService);
                await Logout(dataService);
                // Logger.Info("Response from GetItemByPath " + result.Content.ReadAsStringAsync().Result);
                var resultString = await result.Content.ReadAsStringAsync();
                return result.StatusCode switch
                {
                    HttpStatusCode.NotFound => NotFound(resultString),
                    HttpStatusCode.OK => Ok(resultString),
                    _ => BadRequest(resultString)
                };

            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred getting item " + e.Message);
                return BadRequest("Exception occurred getting item " + e.Message);
            }
        }
      [HttpPost]
      [Route("AddProvider")]
      [ProducesResponseType(StatusCodes.Status200OK)]
      [ProducesResponseType(StatusCodes.Status400BadRequest)]
      [Produces("application/json")]
     //   [HttpPost, Route("AddProvider")]
        public async Task<ActionResult> AddProvider([FromBody] string jsonProvider, [FromHeader(Name="user")] string user, [FromHeader(Name = "url")] string url)
        {
            try
            {
                if (user == null || url == null)
                    return BadRequest("Missing required parameters");
                // login first
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);
                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }
              
                var response = await AddProvider(jsonProvider, dataService);
                Logger.WriteInfo($"Response from AddProvider {await response.Content.ReadAsStringAsync()} for URL: {url} JsonProvider {jsonProvider}");
                await Logout(dataService);
                return response.StatusCode switch
                {
                    HttpStatusCode.Created => Ok(),
                    _ => BadRequest(await response.Content.ReadAsStringAsync())
                };

            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred " + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }
     [HttpPost]
     [Route("UpdateProvider")]
     [ProducesResponseType(StatusCodes.Status200OK)]
     [ProducesResponseType(StatusCodes.Status400BadRequest)]
     [Produces("application/json")]
     public async Task<ActionResult> UpdateProvider([FromBody] string jsonProvider, [FromHeader(Name = "user")] string user, [FromHeader(Name = "url")] string url, [FromHeader(Name = "existingId")] string existingId)
        {
            try
            {
                if (user==null || url == null || existingId == null)
                    return BadRequest("Missing required parameters");

                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                var dataService = new DataService();
                dataService.SetBaseUrl(url);
                var success = await Login(sitecoreUser, dataService);
                if (!success)
                {
                    return BadRequest("Login not successful.");
                }
                var response = await UpdateProvider(jsonProvider, existingId, dataService);
                Logger.WriteInfo($"Response from UpdateProvider {await response.Content.ReadAsStringAsync()} for URL: {url} Provider ID: {existingId} JsonProvider {jsonProvider}");
                await Logout(dataService);
                
                return response.StatusCode switch
                {
                    HttpStatusCode.NoContent => Ok(),
                    HttpStatusCode.BadRequest => BadRequest(await response.Content.ReadAsStringAsync()),
                    HttpStatusCode.Forbidden => Forbid(),
                    HttpStatusCode.NotFound => NotFound(),
                    _ => BadRequest(await response.Content.ReadAsStringAsync())
                };

            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred " + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

         private async Task<HttpResponseMessage> UpdateProvider(string provider, string existingId, DataService dataService)
         {
             try
             {
                 var content = new StringContent(
                     provider,
                     Encoding.UTF8,
                     "application/json");
                 var postUri ="sitecore/api/ssc/item/" + existingId + "?database=master";
                 var patchMessage = await dataService.PatchAsync(postUri, content);
                 var result = patchMessage;

                 Logger.WriteInfo($"Result from UpdateProvider {await result.Content.ReadAsStringAsync()}");
                 return result;
             }
             catch (Exception e)
             {
                   Logger.WriteError($"Exception updating provider {provider} : {e.Message}");
                   throw;
             }
         }
         private async Task<HttpResponseMessage> AddProvider(string provider,DataService dataService)
        {
            try
            {
                var content = new StringContent(
                    provider,
                    Encoding.UTF8,
                    "application/json");
                var postUri = "sitecore/api/ssc/item/sitecore/content/Search/Providers?database=master";
                //var postUri = "sitecore/api/ssc/item/sitecore/content/Search/Providers";
                var postMessage = await dataService.Post(postUri, content);
                return postMessage;
            }
            catch (Exception e)
            {
                Logger.WriteError("Exception occurred AddProvider() " + e.Message);
                throw;
            }
        }
         private async Task<HttpResponseMessage> GetItemByPath(string baseUrl, string path, string id, DataService dataService)
        {
            var item = await dataService.Get(baseUrl + "sitecore/api/ssc/item/?database=master&path=" + path + "/" + id);
            return item;
        }
        private async Task<HttpResponseMessage> GetItemById(string baseUrl, string id, DataService dataService)
        {
            var item = await dataService.Get(baseUrl + "sitecore/api/ssc/item/" + id + "?database=master");

            // get parent path
            return item;
        }
        private async Task<string> GetChildren(string baseUrl, string parentId, DataService dataService)
        {
            var itemResult =
                await dataService.Get(baseUrl + "sitecore/api/ssc/item/" + parentId + "/children?database=master");
            var responseMessage = itemResult;
            if (!responseMessage.IsSuccessStatusCode) return null;
            var result = await responseMessage.Content.ReadAsStringAsync();
            return result;

        }
        private async Task<bool> DeleteProvider(string id, DataService dataService)
        {
            var deleteMessage = await dataService.Delete("sitecore/api/ssc/item/" + id + "?database=master");

            var responseMessage = deleteMessage;
            return responseMessage.IsSuccessStatusCode;
        }
        private async Task<bool> Login(SitecoreUser user, DataService dataService)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(user),
                Encoding.UTF8,
                "application/json");
            var response = await dataService.Post("sitecore/api/ssc/auth/login", content);
            Logger.WriteInfo($"Response from Login {await response.Content.ReadAsStringAsync()} ");
            return response.IsSuccessStatusCode;
        }
        private async Task<bool> Logout(DataService dataService)
        {
            var response = await dataService.Post("sitecore/api/ssc/auth/logout");
            Logger.WriteInfo($"Response from Logout {await response.Content.ReadAsStringAsync()} ");
            return response.IsSuccessStatusCode;
        }
    }
}
