﻿using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Config;
using NLog.Targets;
using SitecoreMicroservice.Helper;
using SitecoreMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SitecoreMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PPMobileController : ControllerBase
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public PPMobileController()
        {
            LoggingConfiguration config = new NLog.Config.LoggingConfiguration();
            FileTarget logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"PPMobile-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
        }

        [HttpGet]
        public string Get()
        {
            const string message = "PP Mobile piece for Sitecore microservice started .....";
            return message;
        }

        [HttpGet]
        [Route("GetPPInfoHubContent")]
        public ActionResult GetPPInfoHubContent()
        {
            var headers = Request.Headers;

            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var parentId)) return BadRequest("Invalid header value returned...");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(url) };

                var success = LoginUser.Login(client, sitecoreUser);
                if (!success)
                {
                    Logger.Error("Login not successful for GET PP Info Hub Content.");
                    return BadRequest("Login not successful.");
                }

                var result = GetChildrenWithStandardFields(client, parentId);
                if (result == null) return BadRequest("No Children returned");

                var array = JArray.Parse(result);
                var infoHubArticleList = new List<InfoHubArticle>();
                foreach (var v in array.Children())
                {
                    var i = new InfoHubArticle
                    {
                        ItemID = v.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemID")?.Value.ToString(),
                        Title = v.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemName")?.Value.ToString(),
                        Heading = v.Children<JProperty>().FirstOrDefault(x => x.Name == "Heading")?.Value.ToString(),
                        SubHeading = v.Children<JProperty>().FirstOrDefault(x => x.Name == "SubHeading")?.Value
                            .ToString(),
                        CreateDate = PopulateDate(v.Children<JProperty>().FirstOrDefault(x => x.Name == "__Created")?.Value.ToString()),
                        Body = v.Children<JProperty>().FirstOrDefault(x => x.Name == "Body")?.Value.ToString()
                    };

                    var mainImageId = ExtractImageGuid(v.Children<JProperty>()
                        .FirstOrDefault(x => x.Name == "Main Image")?.Value.ToString());

                    if (!string.IsNullOrEmpty(mainImageId))
                    {
                        var item = GetItemById(client, mainImageId);

                        if (item.StatusCode == HttpStatusCode.OK)
                        {
                            var itemContenet = item.Content.ReadAsStringAsync().Result;

                            var data = JObject.Parse(itemContenet);

                            i.MainImageUrl = url + data.Children<JProperty>()
                                .FirstOrDefault(x => x.Name == "ItemMedialUrl")?.Value.ToString();

                            var webClient = new WebClient();
                            i.MainImageByte = webClient.DownloadData(i.MainImageUrl);


                        }
                    }

                    infoHubArticleList.Add(i);
                }

                return Ok(infoHubArticleList);
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetPPInfoHubContent" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetPPEventsContent")]
        public ActionResult GetPPEventsContent()
        {
            var headers = Request.Headers;

            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var parentId)) return BadRequest("Invalid header value returned...");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(url) };

                var success = LoginUser.Login(client, sitecoreUser);
                if (!success)
                {
                    Logger.Error("Login not successful for Get PP Events Content.");
                    return BadRequest("Login not successful.");
                }

                var result = GetChildrenWithStandardFields(client, parentId);
                if (result == null) return BadRequest("No Children returned");

                var array = JArray.Parse(result);
                var eventsList = new List<Events>();
                foreach (var v in array.Children())
                {
                    try
                    {
                        var e = new Events
                        {
                            ItemID = v.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemID")?.Value.ToString(),
                            Title = v.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemName")?.Value.ToString(),
                            Location = v.Children<JProperty>().FirstOrDefault(x => x.Name == "Location")?.Value
                                .ToString(),
                            Details =
                                v.Children<JProperty>().FirstOrDefault(x => x.Name == "Details")?.Value.ToString(),
                            StartDate = PopulateDate(v.Children<JProperty>().FirstOrDefault(x => x.Name == "Start")
                                ?.Value
                                .ToString()),
                            EndDate = PopulateDate(v.Children<JProperty>().FirstOrDefault(x => x.Name == "End")?.Value.ToString())
                        };

                        if (!string.IsNullOrEmpty(e.StartDate))
                        {
                            var date = Convert.ToDateTime(e.StartDate);

                            e.DateStartMonth = date.Month.ToString();
                            e.DateStartDay = date.Day;
                        }


                        eventsList.Add(e);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception occurred for GetPPEventsContent" + e.Message);
                    }
                }

                return Ok(eventsList);
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetPPEventsContent" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetHelpfulLinks")]
        public ActionResult GetHelpfulLinks()
        {
            var headers = Request.Headers;

            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var parentId)) return BadRequest("Invalid header value returned...");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);


                using var client = new HttpClient() { BaseAddress = new Uri(url) };

                var success = LoginUser.Login(client, sitecoreUser);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Helpful Links.");
                    return BadRequest("Login not successful.");
                }

                var result = GetChildrenWithStandardFields(client, parentId);
                if (result == null) return BadRequest("No Children returned");

                var array = JArray.Parse(result);
                var quickLinkList = new List<QuickLink>();
                foreach (var v in array.Children())
                {
                    try
                    {
                        var q = new QuickLink
                        {
                            Title = v.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemName")?.Value.ToString(),
                            Url = v.Children<JProperty>().FirstOrDefault(x => x.Name == "Link")?.Value
                                .ToString(),
                        };

                        quickLinkList.Add(q);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception occurred for GetHelpfulLinks" + e.Message);
                    }
                }

                return Ok(quickLinkList);
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetHelpfulLinks" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetUserQuickLinks")]
        public ActionResult GetUserQuickLinks()
        {
            var headers = Request.Headers;
            var quickLinkList = new List<QuickLink>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var echoId) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get User QuickLinks.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url, "echoid|" + echoId);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        var qLinks = JArray.Parse(objText);

                        foreach (var v in qLinks.Children())
                        {
                            try
                            {
                                var q = new QuickLink
                                {
                                    Title = v.Children<JProperty>().FirstOrDefault(x => x.Name == "linkTitle")?.Value.ToString(),
                                    Url = v.Children<JProperty>().FirstOrDefault(x => x.Name == "linkUrl")?.Value
                                        .ToString(),
                                };

                                quickLinkList.Add(q);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                        }
                    }
                    return Ok(quickLinkList);
                }
                else
                {
                    Logger.Error("No providers were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetUserQuickLinks" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetPPHelpContent")]
        public ActionResult GetPPHelpContent()
        {
            var headers = Request.Headers;

            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var id)) return BadRequest("Missing input values....");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                // login first
                using var client = new HttpClient() { BaseAddress = new Uri(url) };
                var success = LoginUser.Login(client, sitecoreUser);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Help Content.");
                    return BadRequest("Login not successful.");
                }

                var item = GetItemById(client, id);

                if (item.StatusCode == HttpStatusCode.OK)
                {
                    var itemContent = item.Content.ReadAsStringAsync().Result;

                    var data = JObject.Parse(itemContent);

                    var helpContent = data.Children<JProperty>().FirstOrDefault(x => x.Name == "Rich Text Content")?.Value
                        .ToString();
                    return Ok(helpContent);
                }
                else
                {
                    Logger.Error("Status Code was not a 200 value for pulling the help content from sitecore. ::" + item.StatusCode);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetPPHelpContent" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetProviders")]
        public ActionResult GetProviders()
        {
            var headers = Request.Headers;
            var providers = new List<Provider>();
            var results = new List<Provider>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Providers.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        providers = (List<Provider>)JsonConvert.DeserializeObject(objText, typeof(List<Provider>), settings);
                        results = providers.Take(100).ToList();
                    }
                    return Ok(results);
                }
                else
                {
                    Logger.Error("No providers were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetProviders" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetAllProviders")]
        public ActionResult GetAllProviders()
        {
            var headers = Request.Headers;
            var providers = new List<Provider>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Providers.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        providers = (List<Provider>)JsonConvert.DeserializeObject(objText, typeof(List<Provider>), settings);
                    }
                    return Ok(providers);
                }
                else
                {
                    Logger.Error("No providers were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetProviders" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetLanguages")]
        public ActionResult GetLanguages()
        {
            var headers = Request.Headers;
            var languages = new List<Language>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Languages.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        languages = (List<Language>)JsonConvert.DeserializeObject(objText, typeof(List<Language>), settings);
                    }
                    return Ok(languages);
                }
                else
                {
                    Logger.Error("No languages were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetLanguages" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetSpecialties")]
        public ActionResult GetSpecialties()
        {
            var headers = Request.Headers;
            var specialties = new List<Specialty>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Specialties.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        specialties = (List<Specialty>)JsonConvert.DeserializeObject(objText, typeof(List<Specialty>), settings);
                    }
                    return Ok(specialties);
                }
                else
                {
                    Logger.Error("No specialties were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetSpecialties" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetHospitals")]
        public ActionResult GetHospitals()
        {
            var headers = Request.Headers;
            var hospitals = new List<_HospitalMapping>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Hospitals.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        hospitals = (List<_HospitalMapping>)JsonConvert.DeserializeObject(objText, typeof(List<_HospitalMapping>), settings);
                    }
                    return Ok(hospitals);
                }
                else
                {
                    Logger.Error("No hospitals were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetHospitals" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetSpecificProviderById")]
        public ActionResult GetSpecificProviderById()
        {
            var headers = Request.Headers;
            var providers = new List<Provider>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl) || !headers.TryGetValue("id", out var id)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Specifi cProvider By Id.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        providers = (List<Provider>)JsonConvert.DeserializeObject(objText, typeof(List<Provider>), settings);
                    }

                    var provider = providers.FirstOrDefault(x => x.Id == Convert.ToInt32(id));
                    return Ok(provider);
                }
                else
                {
                    Logger.Error("No provider was returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetSpecificProviderById" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        //Health Science Library Options
        [HttpGet]
        [Route("GetMedicalLibraryOptions")]
        public ActionResult GetMedicalLibraryOptions()
        {
            var headers = Request.Headers;
            var medicalLibraryOptions = new List<MedicalLibraryOptions>();
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Medical Library Options.");
                    return BadRequest("Login not successful.");
                }

                var httpWebResponse = GetDataFromMicroservice(url);

                if (httpWebResponse != null)
                {
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        medicalLibraryOptions = (List<MedicalLibraryOptions>)JsonConvert.DeserializeObject(objText, typeof(List<MedicalLibraryOptions>), settings);
                    }
                    return Ok(medicalLibraryOptions);
                }
                else
                {
                    Logger.Error("No Medical Library Options were returned from the microservice :: " + url);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetMedicalLibraryOptions" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetMedicalLibraryContent")]
        public ActionResult GetMedicalLibraryContent()
        {
            var headers = Request.Headers;

            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var id) || !headers.TryGetValue("welcomeId", out var welcomeId)) return BadRequest("Invalid header value returned...");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(url) };

                var success = LoginUser.Login(client, sitecoreUser);
                if (!success)
                {
                    Logger.Error("Login not successful for GET Medical Library Content.");
                    return BadRequest("Login not successful.");
                }

                var mlc = new MedicalLibaryContent();

                //Get Medical Library Content
                var item1 = GetItemById(client, welcomeId);
                var itemContenet1 = item1.Content.ReadAsStringAsync().Result;

                var data1 = JObject.Parse(itemContenet1);

                mlc.WelcomeContent = url + data1.Children<JProperty>()
                    .FirstOrDefault(x => x.Name == "SubHeading")?.Value.ToString();

                // Get Library Search Request & Request Journal Articles Content
                var result1 = GetChildrenWithStandardFields(client, welcomeId);
                if (result1 == null) return BadRequest("No Children returned");

                var array1 = JArray.Parse(result1);

                foreach (var v in array1.Children())
                {
                    switch (v.Children<JProperty>()
                        .FirstOrDefault(x => x.Name == "DisplayName")?.Value.ToString())
                    {
                        case "Request Journal Articles":
                            mlc.RequestJournalContent = v.Children<JProperty>()
                                .FirstOrDefault(x => x.Name == "SubHeading")?.Value.ToString();
                            break;
                        case "Library Search Request":
                            mlc.LibrarySerchContent = v.Children<JProperty>()
                                .FirstOrDefault(x => x.Name == "SubHeading")?.Value.ToString();
                            break;
                    }
                }

                var result = GetChildrenWithStandardFields(client, id);
                if (result == null) return BadRequest("No Children returned");

                var array = JArray.Parse(result);
                mlc._CarouselList = new List<CarouselList>();

                foreach (var v in array.Children())
                {

                    // Populating the carousel List

                    var c = new CarouselList();

                    //var mainImageId = v.Children<JProperty>()
                    //    .FirstOrDefault(x => x.Name == "ItemID")?.Value.ToString();

                    var mainImageId = ExtractImageGuid(v.Children<JProperty>()
                        .FirstOrDefault(x => x.Name == "BackgroundImage")?.Value.ToString());

                    c.Id = mainImageId;

                    c.Name = v.Children<JProperty>()
                        .FirstOrDefault(x => x.Name == "ItemName")?.Value.ToString();

                    c.Link = ExtractUrl(v.Children<JProperty>()
                        .FirstOrDefault(x => x.Name == "Link")?.Value.ToString());

                    if (!string.IsNullOrEmpty(mainImageId))
                    {
                        var item = GetItemById(client, mainImageId);

                        if (item.StatusCode == HttpStatusCode.OK)
                        {
                            var itemContenet = item.Content.ReadAsStringAsync().Result;

                            var data = JObject.Parse(itemContenet);

                            c.MainImageUrl = url + data.Children<JProperty>()
                                .FirstOrDefault(x => x.Name == "ItemMedialUrl")?.Value.ToString();

                            var webClient = new WebClient();
                            c.MainImageByte = webClient.DownloadData(c.MainImageUrl);
                        }
                    }

                    mlc._CarouselList.Add(c);
                }

                return Ok(mlc);
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetPPInfoHubContent" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpPost]
        [Route("SubmitMedicalLibraryRequest")]
        public ActionResult SubmitMedicalLibraryRequest()
        {
            var headers = Request.Headers;
            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("subject", out var subject) || !headers.TryGetValue("sitecoreUrl", out var sitecoreUrl) || !headers.TryGetValue("body", out var body) || !headers.TryGetValue("from", out var from) || !headers.TryGetValue("to", out var to) || !headers.TryGetValue("baseUrl", out var baseUrl)) return BadRequest("Invalid header value returned...");

                var usr = JsonConvert.DeserializeObject<SitecoreUser>(user);

                using var client = new HttpClient() { BaseAddress = new Uri(sitecoreUrl) };
                var success = LoginUser.Login(client, usr);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Medical Library Options.");
                    return BadRequest("Login not successful.");
                }

                var m = new Mail
                {
                    To = to,
                    From = from,
                    Body = body,
                    IsBodyHtml = true,
                    Subject = subject
                };

                var res = SendMail.SendMailMessage(m, baseUrl);

                return Ok();
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetMedicalLibraryOptions" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        [HttpGet]
        [Route("GetPhysicianCornerDetails")]
        public ActionResult GetPhysicianCornerDetails()
        {
            var headers = Request.Headers;

            try
            {
                if (!headers.TryGetValue("user", out var user) || !headers.TryGetValue("url", out var url) ||
                    !headers.TryGetValue("id", out var id) || !headers.TryGetValue("path", out var path) || !headers.TryGetValue("externalPath", out var externalPath)) return BadRequest("Missing input values....");
                var sitecoreUser = JsonConvert.DeserializeObject<SitecoreUser>(user);
                // login first
                using var client = new HttpClient() { BaseAddress = new Uri(url) };
                var success = LoginUser.Login(client, sitecoreUser);
                if (!success)
                {
                    Logger.Error("Login not successful for Get Help Content.");
                    return BadRequest("Login not successful.");
                }

                var result = GetChildrenWithStandardFields(client, id);
                if (result == null) return BadRequest("No Children returned");

                var array = JArray.Parse(result);

                var pcList = new List<PhysicianCorner>();

                foreach (var v in array.Children())
                {
                    var pc = new PhysicianCorner();
                    pc._PhysicianCornerDetails = new List<PhysicianCornerDetails>();

                    var pcDetails = new PhysicianCornerDetails();
                    pc.Name = v.Children<JProperty>().FirstOrDefault(x => x.Name == "DisplayName")?.Value.ToString();
                    pc.ItemType = v.Children<JProperty>().FirstOrDefault(x => x.Name == "TemplateName")?.Value.ToString();

                    var pId = v.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemID")?.Value.ToString();

                    var item = GetItemById(client, pId);
                    var itemContent = item.Content.ReadAsStringAsync().Result;

                    var data = JObject.Parse(itemContent);

                    if (pc.Name == "Events & Announcements") continue;

                    {
                        switch (pc.Name)
                        {
                            case "Baptist Physician Partners - BPP":
                                pcDetails.Detail = ExtractUrl(data.Children<JProperty>()
                                    .FirstOrDefault(x => x.Name == "Link")
                                    ?.Value.ToString());
                                break;
                            case "Key Contacts":
                                pcDetails._KeyContactsContent = FormatKeyDetails(data.Children<JProperty>()
                                     .FirstOrDefault(x => x.Name == "Body")
                                     ?.Value.ToString());
                                break;
                            case "Leadership by Facility":
                                pcDetails._LeadershipByFacilityContent = GetLeadershipByFacilityContent(data, url, sitecoreUser, path, externalPath);
                                break;
                        }
                        pc._PhysicianCornerDetails.Add(pcDetails);

                        pcList.Add(pc);
                    }
                }

                return Ok(pcList);
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred for GetPhysicianCornerDetails" + e.Message);
                return BadRequest("Exception occurred " + e.Message);
            }
        }

        private LeadershipByFacilityContent GetLeadershipByFacilityContent(JObject data, string url, SitecoreUser sitecoreUser, string path, string externalPath)
        {
            var lfc = new LeadershipByFacilityContent();

            try
            {
                var leadershipData = data.Children<JProperty>().FirstOrDefault(x => x.Name == "Body")?.Value.ToString();

                var leadershipDataList = leadershipData.Split("</p>");

                lfc._LeadershipByFacilityContentList = new List<LeadershipByFacilityContentList>();
                var header = leadershipDataList[0].Replace("<p>", "");
                lfc.Header = header;

                foreach (var item in leadershipDataList.Skip(1))
                {
                    var htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(item);

                    var node = htmlDoc.DocumentNode.InnerText;

                    var l = new LeadershipByFacilityContentList();
                    var name = node.Replace("\n", "");
                    l.FacilityName = name;

                    if (item.Contains("<a href="))
                    {
                        var hrefCount = htmlDoc.DocumentNode.SelectNodes("//a").Count;
                        var otherNode = htmlDoc.DocumentNode.SelectNodes("//a");

                        if (hrefCount == 1)
                        {
                            var resultData = string.Empty;

                            foreach (var node1 in otherNode)
                            {
                                resultData = node1.Attributes["href"].Value;
                            }

                            var itemUrl = url + resultData;

                            resultData = resultData.Replace("-/media/", "");
                            resultData = resultData.Replace(".ashx", "");

                            if (!string.IsNullOrEmpty(resultData))
                            {
                                var client = new HttpClient() { BaseAddress = new Uri(url) };

                                var success = LoginUser.Login(client, sitecoreUser);
                                if (!success)
                                {
                                    Logger.Error("Login not successful for GetLeadershipByFacilityContent.");
                                }

                                var mediaId = Guid.Parse(resultData).ToString();

                                var result = GetItemById(client, mediaId);
                                var itemContent = result.Content.ReadAsStringAsync().Result;

                                var dataResult = JObject.Parse(itemContent);

                                var fileName = dataResult.Children<JProperty>()
                                    .FirstOrDefault(x => x.Name == "DisplayName")?.Value.ToString();

                                var fileType = dataResult.Children<JProperty>()
                                    .FirstOrDefault(x => x.Name == "Extension")?.Value.ToString();

                                Logger.Info("Generating stream data. :: " + itemUrl + " :: " + fileName + " : : " + fileType + " :: " + path + " :: " + externalPath);
                                var generatedUrl = GenerateStream(itemUrl, fileName, fileType, path, externalPath);
                                l.PdfUrl = generatedUrl;
                                Logger.Info("pdf url. :: " + l.PdfUrl);
                                l.FileName = fileName + "." + fileType;
                                Logger.Info("fileName. :: " + l.FileName);
                                //var webClient = new WebClient();
                                //var newResults = webClient.DownloadData(mediaUrl);

                                //if (newResults != null)
                                //{
                                //    GenerateStream(itemUrl);
                                //    //l.PdfByte = newResults;
                                //}
                            }
                        }
                    }
                    lfc._LeadershipByFacilityContentList.Add(l);
                }
            }
            catch (Exception e)
            {
                Logger.Error("Error retrieving data from GetLeadershipByFacilityContent.:: " + e.Message);
                lfc = null;
            }
            return lfc;
        }

        private string GenerateStream(string itemUrl, string fileName, string fileType, string path, string externalPath)
        {
            var results = "";

            try
            {
                //var bytes = System.Text.Encoding.UTF8.GetBytes(itemUrl);
                var webClient = new WebClient();
                var bytes = webClient.DownloadData(itemUrl);

                var file = fileName + "." + fileType;

                if (Directory.Exists(path))
                {
                    if (!System.IO.File.Exists(file))
                    {
                        System.IO.File.WriteAllBytes(path + file, bytes);
                    }
                }
                results = externalPath + file;
            }
            catch (Exception e)
            {
                Logger.Error("Error retrieving data from GenerateStream.:: " + e.Message);
                results = "";
            }

            return results;
        }

        private List<KeyContactsContent> FormatKeyDetails(string v)
        {
            var detailContent = new List<KeyContactsContent>();

            var detailData = Regex.Split(v, "\n");
            foreach (var item in detailData)
            {
                var dc = new KeyContactsContent();

                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(item);
                //item.ToString().Trim();
                if (item.Contains("<th>"))
                {
                    if (!item.Contains("&nbsp;"))
                    {
                        var node = htmlDoc.DocumentNode.SelectSingleNode("//th");
                        var data = node.InnerText;

                        dc.Name = data;
                        dc.Type = "Title";
                        detailContent.Add(dc);
                    }
                }
                else if (item.Contains("<td>"))
                {
                    if (item.Contains("<a href="))
                    {
                        var hrefCount = htmlDoc.DocumentNode.SelectNodes("//a").Count;
                        var otherNode = htmlDoc.DocumentNode.SelectNodes("//a");

                        if (hrefCount == 1)
                        {
                            var resultData = string.Empty;

                            foreach (var node1 in otherNode)
                            {
                                resultData = node1.Attributes["href"].Value;
                            }
                            if (!string.IsNullOrEmpty(resultData))
                            {
                                var results = resultData.Split(':');
                                var node2 = htmlDoc.DocumentNode.SelectSingleNode("//a");
                                dc.Name = node2.InnerText;
                                dc.Type = results[0];
                                dc.ContactInfo = results[1];
                            }
                        }
                    }
                    else
                    {
                        var node = htmlDoc.DocumentNode.SelectSingleNode("//td");
                        var data = node.InnerText;

                        dc.Name = data;
                        dc.Type = "Text";
                    }

                    detailContent.Add(dc);
                }
                else if (item.Contains("<br />"))
                {
                    if (item.Contains("<a href="))
                    {
                        var hrefCount = htmlDoc.DocumentNode.SelectNodes("//a").Count;
                        var otherNode = htmlDoc.DocumentNode.SelectNodes("//a");

                        if (hrefCount == 1)
                        {
                            var resultData = string.Empty;

                            foreach (var node1 in otherNode)
                            {
                                resultData = node1.Attributes["href"].Value;
                            }
                            if (!string.IsNullOrEmpty(resultData))
                            {
                                var results = resultData.Split(':');
                                var node2 = htmlDoc.DocumentNode.SelectSingleNode("//a");
                                dc.Name = node2.InnerText;
                                dc.Type = results[0];
                                dc.ContactInfo = results[1];
                            }
                        }
                    }
                    else
                    {
                        var data = htmlDoc.DocumentNode.InnerText;

                        dc.Name = data;
                        dc.Type = "Text";
                    }
                    detailContent.Add(dc);

                }
                else if (item.Contains("</td>"))
                {
                    if (item.Contains("<a href="))
                    {
                        var hrefCount = htmlDoc.DocumentNode.SelectNodes("//a").Count;
                        var otherNode = htmlDoc.DocumentNode.SelectNodes("//a");

                        if (hrefCount == 1)
                        {
                            var resultData = string.Empty;

                            foreach (var node1 in otherNode)
                            {
                                resultData = node1.Attributes["href"].Value;
                            }
                            if (!string.IsNullOrEmpty(resultData))
                            {
                                var results = resultData.Split(':');
                                var node2 = htmlDoc.DocumentNode.SelectSingleNode("//a");
                                dc.Name = node2.InnerText;
                                dc.Type = results[0];
                                dc.ContactInfo = results[1];
                            }
                        }
                    }
                    else
                    {
                        var data = htmlDoc.DocumentNode.InnerText;

                        dc.Name = data;
                        dc.Type = "Text";
                    }
                    detailContent.Add(dc);

                }
                else if (item.Contains("\n"))
                {
                    var data = htmlDoc.DocumentNode.InnerText;

                    dc.Name = data;
                    dc.Type = "Text";
                    detailContent.Add(dc);
                }
            }

            return detailContent;
        }

        private static string GetChildrenWithStandardFields(HttpClient client, string parentId)
        {
            var itemResult = client.GetAsync("sitecore/api/ssc/item/" + parentId + "/children?database=web&includeStandardTemplateFields=true").Result;
            if (itemResult.StatusCode == HttpStatusCode.OK)
            {
                var result = itemResult.Content.ReadAsStringAsync().Result;
                return result;
            }
            return null;
        }

        private string PopulateDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                var newDate = date.Substring(0, 8);
                var year = Convert.ToInt32(newDate.Substring(0, 4));
                var month = 0;
                if (newDate.Substring(4, 1) == "0")
                {
                    var getMonth = Convert.ToInt32(newDate.Substring(5, 1));
                    month = getMonth;
                }
                else
                {
                    var getMonth = Convert.ToInt32(newDate.Substring(4, 2));
                    month = getMonth;
                }

                var day = 0;

                if (newDate.Substring(6, 1) == "0")
                {
                    var getDay = Convert.ToInt32(newDate.Substring(7, 1));
                    day = getDay;
                }
                else
                {
                    var getDay = Convert.ToInt32(newDate.Substring(6, 2));
                    day = getDay;
                }

                var newTime = date.Substring(9, 6);
                var hour = Convert.ToInt32(newTime.Substring(0, 2));

                var min = 0;
                if (newTime.Substring(2, 1) == "0")
                {
                    var getMin = Convert.ToInt32(newTime.Substring(3, 1));
                    min = getMin;
                }
                else
                {
                    var getMin = Convert.ToInt32(newTime.Substring(2, 2));
                    min = getMin;
                }

                var sec = 0;
                if (newTime.Substring(4, 1) == "0")
                {
                    var getSec = Convert.ToInt32(newTime.Substring(5, 1));
                    sec = getSec;
                }
                else
                {
                    var getSec = Convert.ToInt32(newTime.Substring(4, 2));
                    sec = getSec;
                }

                var dt = new DateTime(year, month, day, hour, min, sec);

                return dt.ToString(CultureInfo.InvariantCulture);
            }

            return "";
        }

        private string ExtractImageGuid(string s)
        {
            Match match = Regex.Match(s, @"{([^\}]*)}");
            if (match.Groups.Count > 1)
            {
                return match.Groups[1].Value;
            }
            return string.Empty;
        }

        private string ExtractUrl(string s)
        {
            Match match = Regex.Match(s, @"http[s]?:\/\/[^\\\""]*");
            return match.Success ? match.Value : string.Empty;
        }

        private static HttpWebResponse GetDataFromMicroservice(string strMicroServiceMethod, string input = "", string method = "GET")
        {
            HttpWebResponse webResponse = null;

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(strMicroServiceMethod);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = method;
                if (method.ToLower().Equals("post"))
                    httpWebRequest.ContentLength = 0;
                httpWebRequest.Timeout = 20000;
                if (!string.IsNullOrEmpty(input))
                {
                    if (input.Contains('^'))
                    {
                        foreach (var strHeaders in input.Split('^'))
                        {
                            if (!strHeaders.Contains('|')) continue;
                            var strHeader = strHeaders.Split('|');
                            httpWebRequest.Headers.Add(strHeader[0].Replace(".", ""), strHeader[1]);
                        }
                    }
                    else if (input.Contains('|'))
                    {
                        var strHeader = input.Split('|');
                        httpWebRequest.Headers.Add(strHeader[0].Replace(".", ""), strHeader[1]);
                    }
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return webResponse;
        }

        private static HttpResponseMessage GetItemById(HttpClient client, string id)
        {
            var item = client.GetAsync("sitecore/api/ssc/item/" + id + "?database=web").Result;
            // get parent path
            return item;
        }
    }
}
