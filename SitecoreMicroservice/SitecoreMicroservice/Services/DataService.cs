﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using LoggingService;
using Newtonsoft.Json;

namespace SitecoreMicroservice.Services
{
    public class DataService
    {
        private static LoggerService _logger;
        private readonly HttpClient _httpClient;
        private string _baseUrl;

        public void SetBaseUrl(string url)
        {
            this._baseUrl = url;
        }
        public DataService()
        {
            var httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) => true
            };

            _httpClient = new HttpClient(httpClientHandler)
            {
                Timeout = TimeSpan.FromMinutes(10)
            };
            _logger = new LoggerService("DataService");
        }
        /// <summary>
        /// This Calls any post Async with body parameters
        /// </summary>
        /// <param name="serviceUri">post url</param>
        /// <param name="sc">headers</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> Post(string serviceUri, StringContent sc = null)
        {
            try
            {
                if (_httpClient.BaseAddress == null)
                    _httpClient.BaseAddress = new Uri(_baseUrl);
                var response = await _httpClient.PostAsync(serviceUri, sc);
                _logger.WriteInfo($"Post :: Content: {await response.Content.ReadAsStringAsync()} :: StatusCode: {response.StatusCode}");
                return response;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: DataService::PostAsync({serviceUri}:{JsonConvert.SerializeObject(sc)}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// This is for all Get Calls to the microservice
        /// </summary>
        /// <param name="serviceUri">microservice url</param>
        /// <param name="headers">headers to include</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> Get(string serviceUri, Dictionary<string, string> headers = null)
        {
            try
            {
                if (_httpClient.BaseAddress == null)
                    _httpClient.BaseAddress = new Uri(_baseUrl);
                if (headers != null)
                {
                    foreach (var head in headers)
                    {
                        _httpClient.DefaultRequestHeaders.Add(head.Key, head.Value);
                    }
                }

                var response = await _httpClient.GetAsync(serviceUri);
                return response;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: DataService::GetAsync({serviceUri}:{headers}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// This handles all deletes 
        /// </summary>
        /// <param name="serviceUri">microservice url</param>
        /// <param name="headers">headers to include</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> Delete(string serviceUri, Dictionary<string, string> headers = null)
        {
            try
            {
                if (_httpClient.BaseAddress == null)
                    _httpClient.BaseAddress = new Uri(_baseUrl);
                if (headers != null)
                {
                    foreach (var head in headers)
                    {
                        _httpClient.DefaultRequestHeaders.Add(head.Key, head.Value);
                    }
                }

                var response = await _httpClient.DeleteAsync(serviceUri);
                return response;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: DataService::DeleteAsync({serviceUri}:{headers}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// This calls Patch to api
        /// </summary>
        /// <param name="serviceUri">patch url</param>
        /// <param name="sc">headers</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PatchAsync(string serviceUri, StringContent sc)
        {
            try
            {
                if (_httpClient.BaseAddress == null)
                    _httpClient.BaseAddress = new Uri(_baseUrl);
                var response = await _httpClient.PatchAsync(serviceUri, sc);
                _logger.WriteInfo($"PatchAsync :: Content: {await response.Content.ReadAsStringAsync()} :: StatusCode: {response.StatusCode}");
                return response;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: DataService::PatchAsync({serviceUri}:{sc}) :: {e.Message}");
                throw;
            }
        }
    }
}
